#!/usr/bin/python
import json
import matplotlib.pyplot as plt
from rtree import index

class pointObj:
	def __init__(self, label,data):
		for i in xrange(len(data)):
			self.__dict__[label[i]] = data[i]

def dict_to_object(d):
    if '__class__' in d:
        class_name = d.pop('__class__')
        module_name = d.pop('__module__')
        module = __import__(module_name)
        class_ = getattr(module, class_name)
        args = {}
        for key, value in d.items():
            key = key.encode('utf-8')
            value = value.encode('utf-8')
            args[key] = value
        #print 'INSTANCE ARGS:', args
        inst = class_(args.keys(),args.values())
    else:
        inst = d
    return inst

def range_query(file_idx,minx,miny,maxx,maxy,catg):
	hist = list(file_idx.intersection((minx,miny,maxx,maxy),objects = True))
	result = []
	for ele in hist:
		myobj_instance = json.loads(ele.object, object_hook=dict_to_object)
		if myobj_instance.__dict__['CATEGORY'] == catg:
			result.append(ele.object)
	return result

def knn(file_idx,x,y,n,catg):
	step = 2020
	count = 0
	result = []
	hist = list(file_idx.nearest((x, y, x, y), step, objects=True))
	for ele in hist:
		myobj_instance = json.loads(ele.object, object_hook=dict_to_object)
		if myobj_instance.__dict__['CATEGORY'] == catg:
			# print myobj_instance.__dict__['X_COORD']
			result.append(ele.object)
			count = count+1
		if count >= n:
			break
	return result


			
def main_query(argv):
	p = index.Property()
	file_idx = index.Rtree('rtree')
	if argv[0] == 'range':
		hists = range_query(file_idx,float(argv[1]),float(argv[2]),float(argv[3]),float(argv[4]),argv[5])
		return hists
	elif argv[0] == 'knn':
		hists = knn(file_idx,float(argv[1]),float(argv[2]),int(argv[3]),argv[4])
		return hists
