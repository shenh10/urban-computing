#!/usr/bin/python
import cgitb
import json
cgitb.enable()
import cgi
from  queries import *
data = cgi.FieldStorage()
# class pointObj:
# 	def __init__(self, label,data):
# 		for i in xrange(len(data)):
# 			self.__dict__[label[i]] = data[i]


service = data['service'].value
#service = 'knn'
if service == 'range':
	catg = data['category'].value
	minx = data['minx'].value
	miny = data['miny'].value
	maxx = data['maxx'].value
	maxy = data['maxy'].value
	args = [service,minx, miny,maxx,maxy,catg]
	response = main_query(args)
elif service == 'knn':
	catg = data['category'].value
	posx = data['posx'].value
	posy = data['posy'].value
	number = data['number'].value
	# catg = '010000'
	# posx = '31'
	# posy = '121'
	# number = '4'
	args = [service, posx, posy,number,catg]
	response = main_query(args)
else:
	response = ''

mystatus = "200 OK"
print "Status: %s\n" %mystatus
print json.dumps(response)
# for i in xrange(len(response)):
# 	tmp = json.loads(response[i])
# 	print tmp['X_COORD']