#!/usr/bin/python
import cgitb
cgitb.enable()
import cgi
# import os
import sys
import xlrd
import json
data = cgi.FieldStorage()
service = data['query'].value
# service = 'major'
def main_category(worksheet):
	Main_cat_index = []
	header_row = 0
	headers = [worksheet.cell_value(header_row, i) for i in xrange(worksheet.ncols)]
	for row in xrange(header_row+1, worksheet.nrows):
		col = 0
		cell_type = worksheet.cell_type(row, col)
		if cell_type == xlrd.XL_CELL_EMPTY:
			value = None
		elif cell_type == xlrd.XL_CELL_TEXT:
			value = worksheet.cell_value(row, col).encode('UTF-8')
			Main_cat_index.append(row)
	return Main_cat_index

def mid_category(worksheet, major):
	mid_cat_index = []
	major = major -1
	Main_cat_index = main_category(worksheet)
	left = Main_cat_index[major]
	if major + 1 >= len(Main_cat_index):
		right = worksheet.nrows
	else:
		right = Main_cat_index[major+1]
	for row in xrange(left, right):
		col = 2
		cell_type = worksheet.cell_type(row, col)
		if cell_type == xlrd.XL_CELL_EMPTY:
			value = None
		elif cell_type == xlrd.XL_CELL_TEXT:
			value = worksheet.cell_value(row, col).encode('UTF-8')
			mid_cat_index.append(row)
	return (right,mid_cat_index)

def minor_category(worksheet, major,mid):
	minor_cat_index=[]
	(upRange,mid_cat_index) = mid_category(worksheet, major)
	left = mid_cat_index[mid]
	if mid + 1 >= len(mid_cat_index):
		right = upRange
	else:
		right = mid_cat_index[mid+1]
	for row in xrange(left, right):
		col = 4
		cell_type = worksheet.cell_type(row, col)
		if cell_type == xlrd.XL_CELL_EMPTY:
			value = None
		elif cell_type == xlrd.XL_CELL_TEXT:
			value = worksheet.cell_value(row, col).encode('UTF-8')
			minor_cat_index.append(row)
	return minor_cat_index

data1 = xlrd.open_workbook('cgi-bin/POIYPcat_new.xls')

worksheet=data1.sheets()[0]
props=[]
if service == 'major':
	Main_cat_index=[]
	Main_cat = []
	Main_cat_index= main_category(worksheet)
	for i in Main_cat_index:
		Main_cat.append(worksheet.cell_value(i,1).encode('UTF-8'))

	mystatus = "200 OK"
	print "Status: %s\n" %mystatus
	print json.dumps(Main_cat)
elif service == 'mid':
	main_selected = data['major'].value
	# main_selected = '17'
	mid_cat_index = []
	mid_cat = []
	(right, mid_cat_index) = mid_category(worksheet, int(main_selected))
	for i in mid_cat_index:
		mid_cat.append(worksheet.cell_value(i,3).encode('UTF-8'))
	mystatus = "200 OK"
	print "Status: %s\n" %mystatus
	print json.dumps(mid_cat)
elif service == 'minor':
	main_selected = data['major'].value
	mid_selected = data['mid'].value
	# main_selected = '10'
	# mid_selected = '02'
	minor_cat = []
	minor_cat_index = minor_category(worksheet, int(main_selected), int(mid_selected))
	for i in minor_cat_index:
		minor_cat.append(worksheet.cell_value(i,5).encode('UTF-8'))
	mystatus = "200 OK"
	print "Status: %s\n" %mystatus
	print json.dumps(minor_cat)


