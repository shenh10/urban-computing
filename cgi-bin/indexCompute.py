import matplotlib.pyplot as plt
from rtree import index
import json
import sys
class pointObj:
	def __init__(self, label,data):
		for i in xrange(len(data)):
			self.__dict__[label[i]] = data[i]

def convert_to_builtin_type(obj):  
    # Convert objects to a dictionary of their representation  
    d = { '__class__':obj.__class__.__name__,  
          '__module__':obj.__module__,  
        }  
    d.update(obj.__dict__)  
    return d

print "step 1: load data..."
fileIn =  open("cgi-bin/POI_jiaotong.txt")
iter = 0
lines = fileIn.readlines()
dataSet = [ [] for i in range(len(lines))]
for line in lines:
	lineArr = line.strip().split(',')
	for i in xrange(len(lineArr)):
		dataSet[iter].append(lineArr[i])
	# print dataSet[iter]
	iter = iter + 1
fileIn.close()
latt = dataSet[0].index('Y_COORD')
lott = dataSet[0].index('X_COORD')
ctg = dataSet[0].index('CATEGORY')
print latt
print lott
#dig = plt.figure()

#ax1 = fig.add_subplot(1, 1, 1)  
x = [row[i] for i in {latt} for row in dataSet[1:]]
y = [row[i] for i in {lott} for row in dataSet[1:]]
plt.plot(x,y,'o')
#plt.show()
p = index.Property()
file_idx = index.Rtree('rtree')
for i in xrange(len(x)):
	obj = pointObj(dataSet[0],dataSet[i+1])
	json_obj = json.dumps(obj,  default=convert_to_builtin_type)
	print json_obj
	# obj_back = json.loads(json_obj,object_hook=dict_to_object)
	file_idx.insert(i,(float(x[i]),float(y[i]),float(x[i]),float(y[i])),json_obj)

